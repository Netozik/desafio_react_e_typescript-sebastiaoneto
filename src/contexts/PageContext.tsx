import React, { createContext, ReactNode, useEffect, useState } from "react";

const PageContext = createContext({} as IValues);

interface Props {
  children: ReactNode;
}

interface IValues{
  content: string,
  aba: (contentSelect: string) => void
}

const PageProvider = ({ children }: Props) => {
  const [content, setContent] = useState('Sobre');

  function aba(contentSelect:string){
    setContent(contentSelect);
  }

  return <PageContext.Provider value={{content, aba}}>{children}</PageContext.Provider>;
};

export { PageProvider };
export { PageContext };
