import React, { createContext, ReactNode, useState } from "react";

const HeaderContext = createContext({} as IValues);

interface Props {
  children: ReactNode;
}

interface IValues{
  open: boolean,
  menu: (stateMenu: boolean) => void
}

const HeaderProvider = ({ children }: Props) => {
  const [open, setOpen] = useState(false);

  function menu(stateMenu:boolean){
    setOpen(stateMenu);
  }

  return <HeaderContext.Provider value={{open, menu}}>{children}</HeaderContext.Provider>;
};

export { HeaderProvider };
export { HeaderContext };