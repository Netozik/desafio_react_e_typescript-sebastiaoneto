import React from "react";
import { Footer } from "../components/Footer/Footer";
import { Header } from "../components/Header/Header";
import { InformationsFooter } from "../components/InformationsFooter/InformationsFooter";
import { Main } from "../components/Main/Main";
import { Newsletter } from "../components/Newsletter";
import { ScroolToTop } from "../components/ScroolToTop";
import { WhatsappButton } from "../components/WhatsappButton";

const Home = () => {
  return (
    <div>
      <Header />
      <WhatsappButton />
      <Main />
      <Newsletter />
      <ScroolToTop />
      <InformationsFooter />
      <Footer />
    </div>
  );
};

export { Home };
