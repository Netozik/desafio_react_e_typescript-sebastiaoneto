import React from "react";
import { BreadcrumbsMain } from "./contentsMain/BreadcrumbsMain";
import { MainContent } from "./contentsMain/MainContent";

const Main = () => {
  return (
    <main>
      <BreadcrumbsMain />
      <MainContent />
    </main>
  );
};

export { Main };
