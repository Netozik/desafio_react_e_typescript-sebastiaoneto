import React from "react";
import style from "../../../styles/main/mainContent.module.css";
import { NavigationMain } from "./NavigationMain";

const MainContent = () => {
  return (
    <section className={style["mainContent"]}>
      <div className={style["container"]}>
        <h1 className={style["title"]}>INSTITUCIONAL</h1>
        <NavigationMain />
      </div>
    </section>
  );
};

export { MainContent };
