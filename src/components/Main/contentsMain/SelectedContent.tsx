import React, { useContext } from "react";
import { PageContext } from "../../../contexts/PageContext";
import { About } from "./selectedContent/About";
import { Delivery } from "./selectedContent/Delivery";
import { ExchangeReturn } from "./selectedContent/ExchangeReturn";
import { CustomForm } from "./selectedContent/form/Form";
import { FormPayment } from "./selectedContent/FormPayment";
import { SecurityPrivacy } from "./selectedContent/SecurityPrivacy";

function renderSwitch(param: string) {
  switch (param) {
    case "Sobre":
      return <About />;
    case "Forma de Pagamento":
      return <FormPayment />;
    case "Entrega":
      return <Delivery />;
    case "Troca e Devolução":
      return <ExchangeReturn />;
    case "Segurança e Privacidade":
      return <SecurityPrivacy />;
    default:
      return <CustomForm />;
  }
}

const SelectedContent = () => {
  const { content } = useContext(PageContext);

  return renderSwitch(content);
};

export { SelectedContent };
