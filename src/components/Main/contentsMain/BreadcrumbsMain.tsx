import React from "react";
import style from "../../../styles/main/breadcrumbsMain.module.css";
import {ReactComponent as HomeIcon} from "../../../assets/icons/home-icon.svg";
import {ReactComponent as ArrowIcon} from "../../../assets/icons/arrow-icon.svg";

const BreadcrumbsMain = () => {
  return (
    <section className={style["container"]}>
      <div className={style["breadcrumbs"]}>
        <a className={style["breadcrumbs-link"]} href="#">
          <HomeIcon />
        </a>
        <ArrowIcon />
        <a className={style["breadcrumbs-link"]} href="#">
          INSTITUCIONAL
        </a>
      </div>
    </section>
  );
};

export { BreadcrumbsMain };
