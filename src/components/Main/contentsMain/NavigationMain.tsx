import React from "react";
import style from "../../../styles/main/navigationMain.module.css";
import { LiNavigation } from "./LiNavigation";
import { SelectedContent } from "./SelectedContent";

const NavigationMain = () => {
  return (
    <div className={style["navigationContent"]}>
      <nav className={style["navigation"]}>
        <ul className={style["navigation-list"]}>
          <LiNavigation text="Sobre" />
          <LiNavigation text="Forma de Pagamento" />
          <LiNavigation text="Entrega" />
          <LiNavigation text="Troca e Devolução" />
          <LiNavigation text="Segurança e Privacidade" />
          <LiNavigation text="Contato" />
        </ul>
      </nav>
      <SelectedContent />
    </div>
  );
};

export { NavigationMain };
