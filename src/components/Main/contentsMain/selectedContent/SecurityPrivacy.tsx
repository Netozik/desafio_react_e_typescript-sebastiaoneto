import React from "react";
import style from "../../../../styles/main/contentSelected.module.css";

const SecurityPrivacy = () => {
  return (
    <div className={style["contentSelected"]}>
      <h2 className={style["content-title"]}>Segurança e Privacidade</h2>
      <div className={style["content"]}>
        <p className={style["content-paragraph"]}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.
        </p>
      </div>
    </div>
  );
};

export { SecurityPrivacy };
