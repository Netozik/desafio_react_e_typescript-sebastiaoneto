import React from "react";
import { Formik, Form as FormikForm, FormikHelpers } from "formik";

import FormSchema from "../../../../../schema/FormSchema";
import style from "../../../../../styles/main/form/form.module.css";
import { FieldForm } from "./FieldForm";
import { TermForm } from "./TermForm";

interface IFormikValues {
  name: string;
  email: string;
  cpf: string;
  dataNascimento: string;
  telefone: string;
  instagram: string;
}
const initialValues = {
  name: "",
  email: "",
  cpf: "",
  dataNascimento: "",
  telefone: "",
  instagram: "",
};

const CustomForm = () => {
  const handleFormikSubmit = (
    values: IFormikValues,
    actions: FormikHelpers<IFormikValues>
  ) => {
    console.log(values);
    actions.resetForm();
  };

  return (
    <div className={style["content-selected"]}>
      <Formik
        initialValues={initialValues}
        onSubmit={handleFormikSubmit}
        validationSchema={FormSchema}
      >
        <FormikForm className={style["form"]}>
          <h2 className={style["form__title"]}>PREENCHA O FORMULÁRIO</h2>
          <FieldForm camp="name" lab="Nome" place="Seu nome Completo" />
          <FieldForm camp="email" lab="E-mail" place="Seu e-mail" />
          <FieldForm camp="cpf" lab="CPF" place="000 000 000 00" />
          <FieldForm
            camp="dataNascimento"
            lab="Data de Nascimento"
            place="00.00.0000"
          />
          <FieldForm camp="telefone" lab="Telefone" place="(+00) 00000 0000" />
          <FieldForm camp="instagram" lab="Instagram" place="@seuuser" />
          <TermForm />
          <button className={style["form-button"]} type="submit">
            CADASTRE-SE
          </button>
        </FormikForm>
      </Formik>
    </div>
  );
};

export { CustomForm };
