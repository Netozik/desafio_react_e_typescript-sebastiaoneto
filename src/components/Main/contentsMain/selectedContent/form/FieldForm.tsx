import React, { ReactNode } from "react";
import { ErrorMessage, Field, FieldProps } from "formik";
import MaskedInput, {} from "react-input-mask";
import style from "../../../../../styles/main/form/fieldForm.module.css";

interface IFieldForm {
  lab: string;
  camp: string;
  place: string;
}

const FieldForm = ({ lab, camp, place }: IFieldForm) => {
  return (
    <div className={style["form-inputGroup"]}>
      <label className={style["form__label"]} htmlFor={camp}>
        {lab}
      </label>
      <Field className={style["form__input"]} id={camp} name={camp} placeholder={place} />
      <ErrorMessage
              className={style["form__error"]}
              component="span"
              name={camp}
            />
    </div>
  );
};

export { FieldForm };
