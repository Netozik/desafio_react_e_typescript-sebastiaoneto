import { ErrorMessage, Field } from "formik";
import React from "react";
import style from "../../../../../styles/main/form/termForm.module.css";

const TermForm = () => {
  return (
    <div className={style["form-term"]}>
      <div className={style["form-term__group"]}>
        <label htmlFor="term" className={style["form-term__label"]}>
          <span className={style["form-term__span"]}>*</span> Declaro que li e
          aceito
        </label>
        <div className={style["form-term__input-group"]}>
          <Field
            type="checkbox"
            name="term"
            id="term"
            className={style["form-term__input"]}
          />
          <div className={style["form-term__input--custom"]}></div>
        </div>
      </div>
      <ErrorMessage
        name="term"
        component="span"
        className={style["form-term__error"]}
      />
    </div>
  );
};

export { TermForm };
