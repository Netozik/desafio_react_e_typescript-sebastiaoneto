import React, { useContext } from "react";
import { PageContext } from "../../../contexts/PageContext";
import style from "../../../styles/main/liNavigation.module.css";

interface Itext {
  text: string;
}


const LiNavigation = ({ text }: Itext) => {
  
  const { content, aba } = useContext(PageContext);

  return (
    <li>
      <button onClick={() => aba(text)} className={`${style["item-button"]} ${content === text ? style["is-selected"]:""}`}>{text}</button>
    </li>
  );
};

export { LiNavigation };
