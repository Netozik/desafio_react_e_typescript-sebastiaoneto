import React, { useEffect, useState } from "react";
import { ReactComponent as ArrowToTop } from "../assets/icons/arrowToTop-icon.svg";
import style from "../styles/scroolToTop.module.css";

const ScroolToTop = () => {
  const [pageYPosition, setPageYPosition] = useState(0);

  useEffect(() => {
    window.addEventListener("scroll", getPageYAfterScroll);
  }, []);

  function getPageYAfterScroll() {
    setPageYPosition(window.scrollY);
  }

  function toTop(){
    setPageYPosition(0);
    window.scrollTo(0,0);
  }

  if (pageYPosition > 200) {
    return (
      <aside className={style["scroolToTop"]} onClick={toTop}>
        <ArrowToTop />
      </aside>
    );
  } else return (null);
};

export { ScroolToTop };
