import React, { useContext } from "react";
import { HeaderContext } from "../../../contexts/HeaderContext";
import style from "../../../styles/header/submenuHeader.module.css";

const SubmenuHeader = () => {
  const { open, menu } = useContext(HeaderContext);

  return (
    <div className={style["submenu"]}>
      <div className={style["container"]}>
        <div
          className={`${style["submenu-links"]} ${style["submenu-links--desktop"]}`}
        >
          <a className={style["submenu-link"]} href="#">
            CURSOS
          </a>
          <a className={style["submenu-link"]} href="#">
            SAIBA MAIS
          </a>
        </div>
      </div>
      <div className={style["submenu--mobile"]}>
        <div
          onClick={() => menu(false)}
          className={`${style["submenu--absolute"]} ${
            open == true ? style["is-open"] : ""
          }`}
        ></div>
        <div
          className={`${style["submenu-links"]} ${
            style["submenu-links--mobile"]
          } ${open == true ? style["is-open"] : ""}`}
        >
          <a className={style["submenu-link"]} href="#">
            ENTRAR
          </a>
          <a className={style["submenu-link"]} href="#">
            CURSOS
          </a>
          <a className={style["submenu-link"]} href="#">
            SAIBA MAIS
          </a>
        </div>
      </div>
    </div>
  );
};

export { SubmenuHeader };
