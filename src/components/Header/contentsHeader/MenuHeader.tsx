import React, { useContext } from "react";

import style from "../../../styles/header/menuHeader.module.css";

import logo from "../../../assets/logos/logo-m3Academy.svg";
import { ReactComponent as CartIcon } from "../../../assets/icons/shoppingCart-icon.svg";
import { ReactComponent as MenuIcon } from "../../../assets/icons/menuHamburguer-icon.svg";
import { InputMenuHeader } from "./InputMenuHeader";
import { HeaderContext } from "../../../contexts/HeaderContext";

const MenuHeader = () => {
  const { menu } = useContext(HeaderContext);

  return (
    <div className={style["container"]}>
      <div className={style["menu"]}>
        <div onClick={() => menu(true)} className={style["icon-menu"]}>
          <MenuIcon />
        </div>
        <a href="#" className={style["logo"]}>
          <img src={logo} alt="Logo M3" />
        </a>
        <InputMenuHeader view="desktop" />
        <div className={style["header-links"]}>
          <a className={style["login"]} href="#">
            ENTRAR
          </a>
          <button className={style["cart"]}>
            <CartIcon />
          </button>
        </div>
      </div>
      <InputMenuHeader view="mobile" />
    </div>
  );
};

export { MenuHeader };
