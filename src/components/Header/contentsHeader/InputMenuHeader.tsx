import React from "react";
import style from "../../../styles/header/inputMenuHeader.module.css";
import searchIcon from "../../../assets/icons/search-icon.svg";

interface IView{
  view: string
}

const InputMenuHeader = ({view}: IView) => {
  return (
    <div
        className={`${style["input-search"]} ${style[`input-search--${view}`]}`}
      >
        <input
          className={style["inputHeader"]}
          type="text"
          name="Buscar"
          id="Buscar"
          placeholder="Buscar..."
        />
        <img
          className={style["icon-input"]}
          src={searchIcon}
          alt="Icon de busca"
        />
      </div>
  );
};

export { InputMenuHeader };
