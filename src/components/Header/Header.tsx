import React from "react";
import style from "../../styles/header/header.module.css";
import { SubmenuHeader } from "./contentsHeader/SubmenuHeader";
import { MenuHeader } from "./contentsHeader/MenuHeader";
import { HeaderProvider } from "../../contexts/HeaderContext";

const Header = () => {
  return (
    <HeaderProvider>
      <header className={style["page-header"]}>
        <MenuHeader />
        <SubmenuHeader />
      </header>
    </HeaderProvider>
  );
};

export { Header };
