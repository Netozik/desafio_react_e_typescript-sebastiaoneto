import React from "react";
import { DuvidasFooter } from "./contentsInformationsFooter/Duvidas";
import { FaleConosco } from "./contentsInformationsFooter/FaleConosco";
import { InstitucionalFooter } from "./contentsInformationsFooter/Institucional";
import { SocialMedia } from "./contentsInformationsFooter/SocialMedia";

import style from "../../styles/infosfooter/informationsfooter.module.css";

const InformationsFooter = () => {

  return (
    <section className={style["infosFooter"]}>
      <div className={style["container"]}>
      <InstitucionalFooter />
      <DuvidasFooter />
      <FaleConosco />
      <SocialMedia />
      </div>
    </section>
  );

};

export { InformationsFooter };
