import React, { useState } from "react";
import style from "../../../styles/infosfooter/columns.module.css";

import moreIcon from "../../../assets/icons/more-icon.svg";
import { LiPersonText } from "./LiPersonText";

const FaleConosco = () => {

  const [open, setOpen] = useState(false);

  return (
    <div className={style["infosFooter__column"]}>
      <div
        onClick={() => setOpen(!open)}
        className={style["infosFooter__title--mobile"]}
      >
        <h2 className={style["infosFooter__titles"]}>FALE CONOSCO</h2>
        <span className={style["infosFooter__morelist"]}>
          <img src={moreIcon} alt="Ícone Mais" />
        </span>
      </div>
      <ul
        className={`${style["infosFooter-list--mobile"]} ${
          open == true ? style["open-infos"] : ""
        }`}
      >
        <LiPersonText text="Atendimento ao Consumidor" textType="title" />
        <LiPersonText text="(11) 4159 9504" textType="paragraph" />
        <LiPersonText text="Atendimento ao Cliente" textType="title" />
        <LiPersonText text="(11) 99433-8825" textType="paragraph"/>
      </ul>
    </div>
  );
};

export { FaleConosco };
