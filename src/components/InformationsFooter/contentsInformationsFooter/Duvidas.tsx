import React, { useState } from "react";
import { LiPersonText } from "./LiPersonText";
import style from "../../../styles/infosfooter/columns.module.css";

import moreIcon from "../../../assets/icons/more-icon.svg";

const DuvidasFooter = () => {

  const [open, setOpen] = useState(false);

  return (
    <div className={style["infosFooter__column"]}>
      <div
        onClick={() => setOpen(!open)}
        className={style["infosFooter__title--mobile"]}
      >
        <h2 className={style["infosFooter__titles"]}>DÚVIDAS</h2>
        <span className={style["infosFooter__morelist"]}>
          <img src={moreIcon} alt="Ícone Mais" />
        </span>
      </div>
      <ul
        className={`${style["infosFooter-list--mobile"]} ${
          open == true ? style["open-infos"] : ""
        }`}
      >
        <LiPersonText textType="link" text="Entrega" />
        <LiPersonText textType="link" text="Pagamento" />
        <LiPersonText textType="link" text="Trocas e Devoluções" />
        <LiPersonText textType="link" text="Dúvidas Frequentes" />
      </ul>
    </div>
  );
};

export { DuvidasFooter };
