import React, { useState } from "react";
import { LiPersonText } from "./LiPersonText";
import style from "../../../styles/infosfooter/columns.module.css";

import moreIcon from "../../../assets/icons/more-icon.svg";

const InstitucionalFooter = () => {
  const [open, setOpen] = useState(false);

  return (
    <div className={style["infosFooter__column"]}>
      <div
        onClick={() => setOpen(!open)}
        className={style["infosFooter__title--mobile"]}
      >
        <h2 className={style["infosFooter__titles"]}>INSTITUCIONAL</h2>
        <span className={style["infosFooter__morelist"]}>
          <img src={moreIcon} alt="Ícone Mais" />
        </span>
      </div>
      <ul
        className={`${style["infosFooter-list--mobile"]} ${
          open == true ? style["open-infos"] : ""
        }`}
      >
        <LiPersonText textType="link" text="Quem Somos" />
        <LiPersonText textType="link" text="Política de Privacidades" />
        <LiPersonText textType="link" text="Segurança" />
        <LiPersonText textType="link" text="Seja um Revendedor" classmore="underline" />
      </ul>
    </div>
  );
};

export { InstitucionalFooter };
