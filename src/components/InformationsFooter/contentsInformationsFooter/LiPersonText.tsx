import React from "react";
import style from "../../../styles/infosfooter/liPersonText.module.css";

interface IList {
  text: string;
  textType: string;
  classmore?: string;
}

const LiPersonText = ({ text, textType, classmore }: IList) => {
  switch (textType) {
    case "title":
      return (
        <li className={style["infosFooter-item"]}>
          <h3 className={style["infosFooter-item-contact__title"]}>{text}</h3>
        </li>
      );
    case "link":
      return (
        <li className={style["infosFooter-item"]}>
          <a
            className={`${style["infosFooter-item__link"]} ${
              classmore != null
                ? style[`infosFooter-item__link--${classmore}`]
                : ""
            }`}
            href="#"
            target="_blank"
          >
            {text}
          </a>
        </li>
      );
    default:
      return (
        <li className={style["infosFooter-item"]}>
          <p className={style["infosFooter-item-contact__description"]}>
            {text}
          </p>
        </li>
      );
  }
};

export { LiPersonText };
