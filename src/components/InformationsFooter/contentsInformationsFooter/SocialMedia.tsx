import React from "react";
import { ReactComponent as Facebook } from "../../../assets/icons/facebook-icon.svg";
import { ReactComponent as Instagram } from "../../../assets/icons/instagram-icon.svg";
import { ReactComponent as Twitter } from "../../../assets/icons/twitter-icon.svg";
import { ReactComponent as YouTube } from "../../../assets/icons/youtube-icon.svg";
import { ReactComponent as Linkedin } from "../../../assets/icons/linkedin-icon.svg";

import style from "../../../styles/infosfooter/socialmedia.module.css";

const SocialMedia = () => {
  return (
    <div className={style["infosFooter-socialMedia"]}>
      <div className={style["infosFooter-socialMedia__links"]}>
        <a href="#" target="_blank">
          <Facebook />
        </a>
        <a className={style["infosFooter-socialMedia__item"]} href="#" target="_blank">
          <Instagram />
        </a>
        <a className={style["infosFooter-socialMedia__item"]} href="#" target="_blank">
          <Twitter />
        </a>
        <a className={style["infosFooter-socialMedia__item"]} href="#" target="_blank">
          <YouTube />
        </a>
        <a className={style["infosFooter-socialMedia__item"]} href="#" target="_blank">
          <Linkedin />
        </a>
      </div>
      <p className={style["infosFooter-socialMedia-website"]}>
        <a className={style["infosFooter-socialMedia-website__link"]} href="#" target="_blank">www.loremipsum.com</a>
      </p>
    </div>
  );
};

export { SocialMedia };
