import {
  Formik,
  Form as FormikForm,
  Field,
  ErrorMessage,
  FormikHelpers,
} from "formik";
import React, { useState } from "react";
import * as Yup from "yup";
import style from "../styles/newsletter.module.css";

interface IValue {
  email: string;
}

const Newsletter = () => {
  return (
    <section className={style["newsletter"]}>
      <div className={style["container"]}>
        <Formik
          initialValues={{ email: "" }}
          onSubmit={(value: IValue, actions: FormikHelpers<IValue>) => {
            console.log("Newsletter Assinado!!!");
            actions.resetForm();
          }}
          validationSchema={Yup.object().shape({
            email: Yup.string()
              .email("Email Inválido")
              .required("Campo Obrigatório"),
          })}
        >
          <FormikForm>
            <label className={style["newsletter__label"]} htmlFor="email">
              ASSINE NOSSA NEWSLETTER
            </label>
            <div className={style["newsletter__form-group"]}>
              <Field
                className={style["newsletter__email"]}
                id="email"
                name="email"
                placeholder="E-mail"
              />
              <button className={style["newsletter__button"]} type="submit">
                CADASTRE-SE
              </button>
            </div>
            <ErrorMessage
              className={style["form__error"]}
              component="span"
              name="email"
            />
          </FormikForm>
        </Formik>
      </div>
    </section>
  );
};

export { Newsletter };
