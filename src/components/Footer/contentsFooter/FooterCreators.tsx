import React from "react";
import { ReactComponent as M3 } from "../../../assets/logos/m3-logo.svg";
import { ReactComponent as Vtex } from "../../../assets/logos/vtex-logo.svg";
import style from "../../../styles/footer/creators.module.css";

const FooterCreators = () => {
  return (
    <div className={style["footer-home__creators"]}>
      <a
        href="https://vtex.com/br-pt/"
        target="_blank"
        className={style["footer-home__creators-powered"]}
      >
        Powered by
        <Vtex />
      </a>
      <a
        href="https://m3ecommerce.com/"
        target="_blank"
        className={style["footer-home__creators-developed"]}
      >
        Developed by
        <M3 />
      </a>
    </div>
  );
};

export { FooterCreators };
