import React from "react";
import style from "../../../styles/footer/payments.module.css";

interface IPayment{
  urlImage: string;
  textAlt: string;
  vtexPci?: boolean
}

const ImagePayment = ({urlImage, textAlt, vtexPci} : IPayment) => {
  return (
    <img
          className={`${style["footer-home__payments-band"]} ${vtexPci == true ? style["footer-home__payments-band--vtexPci"]:""}`}
          src={urlImage}
          alt={textAlt}
        />
  );
};

export { ImagePayment };
