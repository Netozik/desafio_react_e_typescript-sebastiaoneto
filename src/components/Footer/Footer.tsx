import React from "react";

import style from "../../styles/footer/footer.module.css";

import Master from "../../assets/images/Master.svg";
import Visa from "../../assets/images/Visa.svg";
import AmericanExpress from "../../assets/images/AmericanExpress.svg";
import Elo from "../../assets/images/Elo.svg";
import Hiper from "../../assets/images/Hiper.svg";
import Paypal from "../../assets/images/PayPal.svg";
import Boleto from "../../assets/images/boleto.svg";
import VtexPci from "../../assets/icons/vtexPci-icon.svg";
import { FooterCreators } from "./contentsFooter/FooterCreators";
import { ImagePayment } from "./contentsFooter/ImagePaymant";

const Footer = () => {
  return (
    <footer className={style["footer-home"]}>
      <p
        className={`${style["footer-home__text"]} ${style["footer-home__text--desktop"]}`}
      >
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor
      </p>
      <div className={style["footer-home__payments"]}>
        <div className={style["footer-home__payments--division"]}>
          <ImagePayment urlImage={Master} textAlt="Bandeira Mastercard" />
          <ImagePayment urlImage={Visa} textAlt="Bandeira Visa" />
          <ImagePayment
            urlImage={AmericanExpress}
            textAlt="Bandeira AmericanExpress"
          />
          <ImagePayment urlImage={Elo} textAlt="Bandeira Elo" />
        </div>
        <div className={style["footer-home__payments--division"]}>
          <ImagePayment urlImage={Hiper} textAlt="Bandeira Hiper" />
          <ImagePayment urlImage={Paypal} textAlt="Bandeira Paypal" />
          <ImagePayment urlImage={Boleto} textAlt="Ícone Boleto" />
          <ImagePayment urlImage={VtexPci} textAlt="Ícone VtexPci" vtexPci={true} />
        </div>
      </div>
      <div
        className={`${style["footer-home__text"]} ${style["footer-home__text--mobile"]}`}
      >
        Lorem ipsum dolor sit amet, consectetur adipiscing elit
      </div>
      <FooterCreators />
    </footer>
  );
};

export { Footer };
