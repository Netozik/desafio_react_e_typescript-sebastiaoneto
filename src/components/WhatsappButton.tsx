import React from "react";
import { ReactComponent as WhatsappIcon } from "../assets/icons/whatsapp-icon.svg";
import style from "../styles/whatsappButton.module.css";

const WhatsappButton = () => {
  return (
    <div>
      <a
        className={style["whatsappButton"]}
        href="https://api.whatsapp.com/send?phone=9999999999999&text=Olá, Tudo Bem?"
        target="_blank"
      >
        <WhatsappIcon />
      </a>
    </div>
  );
};

export { WhatsappButton };
