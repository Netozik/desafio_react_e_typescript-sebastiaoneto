import * as Yup from "yup";
import "yup-phone";
import { parse } from "date-fns";
import CPF from "cpf";

export default Yup.object().shape({
  name: Yup.string().min(3, "Nome inválido").required("Campo obrigatório"),
  email: Yup.string().email("Email inválido").required("Campo obrigatório"),
  telefone: Yup.string()
    .required("Campo obrigatório")
    .phone("BR", true, "Telefone Inválido"),
  instagram: Yup.string().min(3, "Instagram inválido").required("Campo Obrigatório"),
  dataNascimento: Yup.date()
    .transform((value, originalValue, context) => {
      if (context.isType(value)) return value;
      return parse(originalValue, "dd.MM.yyyy", new Date());
    })
    .typeError("Data Inválida")
    .max(new Date(), "Data Inválida")
    .required("Campo Obrigatório"),
  cpf: Yup.string()
    .required("Campo Obrigatório")
    .test("test-cpf", "Cpf inválido", (cpf) => CPF.isValid(cpf!)),
    term: Yup.bool().oneOf([true], 'Termo Obrigatório')
});
